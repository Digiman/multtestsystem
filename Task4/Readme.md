﻿# Задача №3

Все слова в текстовом файле упорядочить по алфавиту. Вывести все слова в алфавитном порядке с указанием количества упоминаний слова в тексте.
Регистр всех слов - нижний (то есть, если есть слов с прописной буквы и такое же со строчной, то считать это один и тем же словом).

Входной файл содержит следующую информацию:

Предложения со словами на английском языке.

Пример:
Lorem labore diam amet vel nostrud ipsum labore sit et eos magna stet ea. Eirmod gubergren ipsum aliquyam et takimata stet assum sanctus gubergren sit justo aliquip et nisl wisi vero amet nonumy. Eleifend dolor et iusto sanctus nostrud sanctus ipsum amet et autem erat nulla feugait. Sea eu dignissim lorem dolor sed sit ut illum amet sea wisi rebum ut consectetuer duo magna feugait ut. Facer amet aliquyam ullamcorper laoreet sit sea eum invidunt stet nisl. Takimata et ea et ex sit sea sanctus dignissim rebum amet ut erat dolor. Et diam eu et. Elitr magna wisi stet dolores sit consequat sit et accusam sed. Vero ut accumsan dolore no duo rebum dignissim vel. Hendrerit diam erat. Clita voluptua consetetur eros dolores laoreet aliquyam ipsum duo id dolor tempor dolores nulla nulla kasd eos. Dolor nostrud lorem ea takimata no lobortis nulla labore dolor duo stet in. Et quis praesent takimata molestie at est magna diam accusam nihil rebum wisi diam. Diam vero euismod sanctus consetetur at sit blandit.

Выходной файл содержит информацию:

Само слово и через пробел - количество его повторений в 

Пример:
accumsan 1
accusam 2
aliquip 1
aliquyam 3
amet 6
assum 1
at 2
autem 1
blandit 1
clita 1
consectetuer 1
consequat 1
consetetur 2
diam 6
dignissim 3
dolor 6
dolore 1
dolores 3
duo 4
ea 3
eirmod 1
eleifend 1
elitr 1
eos 2
erat 3
eros 1
est 1
et 11
eu 2
euismod 1
eum 1
ex 1
facer 1
feugait 2
gubergren 2
hendrerit 1
id 1
illum 1
in 1
invidunt 1
ipsum 4
iusto 1
justo 1
kasd 1
labore 3
laoreet 2
lobortis 1
lorem 3
magna 4
molestie 1
nihil 1
nisl 2
no 2
nonumy 1
nostrud 3
nulla 4
praesent 1
quis 1
rebum 4
sanctus 5
sea 4
sed 2
sit 8
stet 5
takimata 4
tempor 1
ullamcorper 1
ut 5
vel 2
vero 3
voluptua 1
wisi 4

