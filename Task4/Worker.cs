﻿using System.IO;
using CommonLib;
using CommonLib.Elements;

namespace Task4
{
    /// <summary>
    /// Главный рабочий клас приложения для того, чтобы выполнить все операции.
    /// Исполнитель - выполняет создание всех требуемых файлов для помещения задачи в тестовую систему.
    /// </summary>
    public class Worker : WorkerBase
    {
        /// <summary>
        /// Инициализация исполнителя с параметрами.
        /// </summary>
        /// <param name="folderName">Каталог, где будут размещены данные для задачи.</param>
        public Worker(string folderName) : base(folderName)
        {
        }

        /// <summary>
        /// Генерация всех данных по задаче.
        /// </summary>
        public override void GenerateAll()
        {
            var taskDir = CheckAndCreateTaskDirectory();

            var filename = Path.Combine(taskDir, Globals.TasfDefinitionFileName);

            // 1. создание файла с описанием задачи
            _taskWorker.Save(filename);

            // 2. создание файлов с тестами и ответами для них
            var generator = new Generator(taskDir);
            generator.GenerateData(Globals.TestCounts);
        }

        /// <summary>
        /// Инициализация данных для описания задачи.
        /// </summary>
        /// <returns>Возвращает файл с описанием задачи.</returns>
        protected override TaskDefinition InitDefinition()
        {
            // NOTE: здесь вручную приводится описание задачи, ее параметры и прмиеры использования, которы затем пишутся в файл описания.

            var definition = new TaskDefinition();

            // описание проблемы
            definition.ProblemId = "F";

            // Описание задачи
            definition.Title = "Задача о сортировке и подсчете слов";
            definition.Description = "Все слова в текстовом файле упорядочить по алфавиту.";

            // примеры
            definition.Examples.Add(new Example
            {
                Input = "",
                Output = ""
            });

            return definition;
        }
    }
}
