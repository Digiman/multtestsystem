﻿namespace Task4
{
    class Program
    {
        public static void Run()
        {
            Worker tmp = new Worker("task4");
            tmp.GenerateAll();
        }

        static void Main(string[] args)
        {
            Run();
        }
    }
}
