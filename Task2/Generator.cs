﻿using System;
using System.IO;
using CommonLib;
using CommonLib.Generator;

namespace Task2
{
    /// <summary>
    /// Генератор для задачи 2.
    /// </summary>
    public class Generator : GenaratorBase
    {
        /// <summary>
        /// Инициализация генератора входных и выходных файлов для задачи.
        /// </summary>
        /// <param name="path">Путь к кателогу, где будут размещаться файлы.</param>
        public Generator(string path) : base(path)
        {
        }

        /// <summary>
        /// Генерация входного файла с данными длоя решения задачи.
        /// </summary>
        /// <param name="testNumber">Номер теста.</param>
        protected override void GenerateInputFile(int testNumber)
        {
            var filename = GetInputFilename(testNumber);

            var file = new StreamWriter(filename);

            int N = testNumber*30;
            int M = N;
            file.WriteLine("{0} {1}", N, M);

            int[,] matrix = new int[N, M];

            Random r = new Random(100);

            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < M; j++)
                {
                    matrix[i, j] = (r.Next(-100,100)%Globals.MaxHeight) + Globals.MinHeight;
                    file.Write("{0} ", matrix[i, j]);
                }
                file.Write("\n");
            }

            file.Close();
        }

        /// <summary>
        /// Генерация файла с ответами путем решения задачи.
        /// </summary>
        /// <param name="testNumber">Номер теста.</param>
        protected override void GenerateAnswerFile(int testNumber)
        {
            var inputfile = GetInputFilename(testNumber);
            var finder = new SolutionFinder(inputfile);
            finder.FindSolution();
            finder.WriteToFile(GetAnswerFilename(testNumber));
        }
    }
}
