﻿using System.IO;
using CommonLib;
using CommonLib.Elements;

namespace Task2
{
    /// <summary>
    /// Главный рабочий клас приложения для того, чтобы выполнить все операции.
    /// Исполнитель - выполняет создание всех требуемых файлов для помещения задачи в тестовую систему.
    /// </summary>
    public class Worker : WorkerBase
    {
        /// <summary>
        /// Инициализация исполнителя с параметрами.
        /// </summary>
        /// <param name="folderName">Каталог, где будут размещены данные для задачи.</param>
        public Worker(string folderName) : base(folderName)
        { }

        /// <summary>
        /// Генерация всех данных по задаче.
        /// </summary>
        public override void GenerateAll()
        {
            var taskDir = CheckAndCreateTaskDirectory();

            var filename = Path.Combine(taskDir, Globals.TasfDefinitionFileName);

            // 1. создание файла с описанием задачи
            _taskWorker.Save(filename);

            // 2. создание файлов с тестами и ответами для них
            var generator = new Generator(taskDir);
            generator.GenerateData(Globals.TestCounts);
        }

        /// <summary>
        /// Инициализация данных для описания задачи.
        /// </summary>
        /// <returns>Возвращает файл с описанием задачи.</returns>
        protected override TaskDefinition InitDefinition()
        {
            // NOTE: здесь вручную приводится описание задачи, ее параметры и прмиеры использования, которы затем пишутся в файл описания.


            // TODO: Добавить описание задачи №2!
            var definition = new TaskDefinition();

            // описание проблемы
            definition.ProblemId = "D";

            // Описание задачи
            definition.Title = "Задача о поиске вершин и впадин";
            definition.Description = @"Текстовый файл содержит карту вершин в виде двумерной матрицы высот. Значения высот разделяются пробеллами, строки заканчиваются вводом. Необходимо сформировать текстовый файл, содержащий количество вершин и впадин с указанием их координат и глубин.";
            
            // примеры
            definition.Examples.Add(new Example
            {
                Input = "",
                Output = ""
            });

            return definition;
        }
    }
}
