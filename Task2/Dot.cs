﻿namespace Task2
{
    /// <summary>
    /// Точка (вершина или впадина).
    /// </summary>
    public class Dot
    {
        /// <summary>
        /// Координата по X.
        /// </summary>
        public int X { get; set; }
        /// <summary>
        /// Координата по Y.
        /// </summary>
        public int Y { get; set; }
        /// <summary>
        /// Значение высоты.
        /// </summary>
        public int Value { get; set; }

        public Dot(int x, int y, int value)
        {
            X = x;
            Y = y;
            Value = value;
        }
    }
}
