﻿namespace Task2
{
    class Program
    {
        public static void Run()
        {
            Worker tmp = new Worker("task2");
            tmp.GenerateAll();
        }

        static void Main(string[] args)
        {
            Run();
        }
    }
}
