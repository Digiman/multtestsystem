﻿using System;
using System.Collections.Generic;
using System.IO;
using CommonLib.Generator;

namespace Task2
{
    /// <summary>
    /// Класс для реализации поиска решения для рассматриваемой задачи.
    /// </summary>
    public class SolutionFinder : SolutionFinderBase
    {
        /// <summary>
        /// Размерность матрицы 1 (столбцов).
        /// </summary>
        private int M;
        /// <summary>
        /// Размерность матрицы 2 (строк).
        /// </summary>
        private int N;

        /// <summary>
        /// Исходная матрица
        /// </summary>
        private int[,] A;

        /// <summary>
        /// Вершины.
        /// </summary>
        private List<Dot> Vertices;
        /// <summary>
        /// Впадины.
        /// </summary>
        private List<Dot> Dimples;

        /// <summary>
        /// Инициаизация решателя путем передачи ему имени файла с исходынми данными.
        /// </summary>
        /// <param name="filename">Файл и путь к нему с исходными (входными) данными.</param>
        public SolutionFinder(string filename) : base(filename)
        {
            ReadInputFile(filename);

            Vertices = new List<Dot>();
            Dimples = new List<Dot>();
        }

        /// <summary>
        /// Выполнение поиска решения.
        /// </summary>
        public override void FindSolution()
        {
            int x, y;
            for (x = 0; x < N; x++)
                for (y = 0; y < M; y++)
                    FindDots(x, y);
        }

        /// <summary>
        /// Поиск точек вершин и впадин на карте вершин.
        /// </summary>
        /// <param name="x">Координата по X.</param>
        /// <param name="y">Координата по Y.</param>
        private void FindDots(int x, int y)
        {
            if (A[x, y] >= 0)
            {
                Vertices.Add(new Dot(x, y, A[x, y]));
            }
            else
            {
                Dimples.Add(new Dot(x, y, A[x, y]));
            }
        }

        /// <summary>
        /// Запись результатов решения в файл с ответами.
        /// </summary>
        /// <param name="filename">Имя файла с ответами и путь к нему.</param>
        public override void WriteToFile(string filename)
        {
            WriteResultsToFile(filename, Vertices, Dimples);
        }

        /// <summary>
        /// Пишем результаты в файл.
        /// </summary>
        /// <param name="filename">Файл для записи результатов.</param>
        /// <param name="dots1">Список вершин.</param>
        /// <param name="dots2">Список низин.</param>
        private void WriteResultsToFile(string filename, List<Dot> dots1, List<Dot> dots2)
        {
            var file = new StreamWriter(filename);

            // пишем вершины
            file.WriteLine("{0}", dots1.Count);
            foreach (var dot in dots1)
            {
                file.WriteLine("{0} {1} {2} ", dot.X, dot.Y, dot.Value);
            }
            file.WriteLine();

            // пишем низины
            file.WriteLine("{0}", dots2.Count);
            foreach (var dot in dots2)
            {
                file.WriteLine("{0} {1} {2} ", dot.X, dot.Y, dot.Value);
            }

            file.Close();
        }

        /// <summary>
        /// Чтение данных из входного файла.
        /// </summary>
        /// <param name="filename">Файл с даными и путь к нему.</param>
        public void ReadInputFile(string filename)
        {
            var file = new StreamReader(filename);

            var line = file.ReadLine();
            var mas = line.Split(' ');
            N = Convert.ToInt32(mas[0]);
            M = Convert.ToInt32(mas[1]);

            A = new int[N, M];

            for (int i = 0; i < N; i++)
            {
                line = file.ReadLine();
                var lineData = line.Split(' ');
                for (int j = 0; j < M; j++)
                {
                    A[i, j] = Convert.ToInt32(lineData[j]);
                }
            }

            file.Close();
        }
    }
}
