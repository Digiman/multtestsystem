﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using CommonLib.Elements;

namespace CommonLib.Helpers
{
    /// <summary>
    /// Класс для работы с XML-файлом описания задачи для тестовой системы.
    /// </summary>
    public static class XmlWorker
    {
        /*  пример файлс для работы здесь!!
         * <?xml version="1.0" encoding="utf-8" ?>
        <problem
           package = "ru.ejudge.multic1test_contest"
           id = "A"
           type = "standard">
          <statement language="ru_RU">
            <title>Задача "Сглаживание"</title>
            <description>
		        <p>Имеется двумерная матрица размерностью NxM, представляющая собой карту высот. Необходимо сформировать новую матрицу, где вместо значения высоты для каждой точки будет находится её среднее значение для окрестности радиусом R.</p>
		        <p>На первой строке ввода находятся 3 числа: N, M, R.</p>
		        <p>Далее идут N строк, по M чисел каждая.</p>
		        <p>Все числа не отрицательные, не превышают 240.</p>
		        <p>На выходе матрица NxM.</p>
		        <p>&nbsp;</p>
		        <p>Проверка попадания точки в окружность радиуса R, расположенной в начале координат:</p>
 		        <p><pre>(x*x + y*y) &lt; R*R</pre></p>
            </description>
          </statement>
          <examples>
            <example>
        <input>
        5 5 2
        0 0 0 0 0
        0 3 5 3 0
        0 5 7 5 0
        0 3 5 3 0
        0 0 0 0 0
        </input>
        <output>
        0 1 1 1 0 
        1 2 3 2 1 
        1 3 4 3 1 
        1 2 3 2 1 
        0 1 1 1 0
        </output>
            </example>
          </examples>
        </problem>
         */

        /// <summary>
        /// Генерация XML-файла с описанием задачи для тестовой системы.
        /// </summary>
        /// <param name="filename">Имя файла с результатом.</param>
        /// <param name="definition">Описание задачи.</param>
        public static void GenerateStatementFile(string filename, TaskDefinition definition)
        {
            var file = new XmlTextWriter(filename, Encoding.UTF8);

            // начало документа
            file.WriteStartDocument();
            
            file.WriteStartElement("problem"); // <problem>
            file.WriteStartAttribute("package"); // package=""
            file.WriteValue(definition.ProblemPackage);
            file.WriteStartAttribute("id"); // id=""
            file.WriteValue(definition.ProblemId);
            file.WriteStartAttribute("type"); // type=""
            file.WriteValue(definition.ProblemType);

            // описание задачи
            file.WriteStartElement("statement"); // <statement>
            file.WriteStartAttribute("language"); // language=""
            file.WriteValue(definition.StatementLanguage);

            file.WriteStartElement("title"); // <title>
            file.WriteString(definition.Title);
            file.WriteEndElement(); // </title>

            file.WriteStartElement("description"); // <description>
            file.WriteString(definition.Description);
            file.WriteEndElement(); // </description>

            file.WriteEndElement(); // </statement>

            // примеры
            file.WriteStartElement("examples"); // <examples>
            foreach (var example in definition.Examples)
            {
                file.WriteStartElement("examples"); // <example>

                file.WriteStartElement("input"); // <input>
                file.WriteString(example.Input);
                file.WriteEndElement(); // </input>

                file.WriteStartElement("output"); // <output>
                file.WriteString(example.Output);
                file.WriteEndElement(); // </output>

                file.WriteEndElement(); // </example>
            }
            file.WriteEndElement(); // </examples>

            // завершение главного блока
            file.WriteEndElement(); // </problem>

            file.Close();
        }

        /// <summary>
        /// Генерация XML-файла с описанием задачи для тестовой системы.
        /// </summary>
        /// <param name="filename">Имя файла с результатом.</param>
        /// <returns>Возвращает описание задачи, полученное из файла.</returns>
        public static TaskDefinition LoadFromStatementFile(string filename)
        {
            // TODO: написать чтение данных из XML-файла с описанием задачи (statement.xml)

            return null;
        }
    }
}
