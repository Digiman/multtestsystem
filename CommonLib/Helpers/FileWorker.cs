﻿using System.IO;

namespace CommonLib.Helpers
{
    /// <summary>
    /// Класс для реализации общих действий при работе с файлами данных.
    /// </summary>
    public static class FileWorker
    {
        /// <summary>
        /// Запись вектора (одномерного массива) в файл.
        /// </summary>
        /// <typeparam name="T">Тип данных для элементов массива.</typeparam>
        /// <param name="array">Сам массив с данными.</param>
        /// <param name="filename">Имя файла для записи.</param>
        public static void WriteDataToFile<T>(T[] array, string filename)
        {
            var file = new StreamWriter(filename);

            for (int i = 0; i < array.GetLength(0); i++)
            {
                file.WriteLine(array[i]);
            }

            file.Close();
        }

        /// <summary>
        /// Запись матрицы (двухмерного массива) в файл.
        /// </summary>
        /// <typeparam name="T">Тип данных для элементов массива.</typeparam>
        /// <param name="array">Сама матрица с данными.</param>
        /// <param name="filename">Имя файла для записи.</param>
        public static void WriteDataToFile<T>(T[,] array, string filename)
        {
            var file = new StreamWriter(filename);

            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    file.Write("{0} ",array[i, j]);
                }
                file.Write("\n");
            }

            file.Close();
        }
    }
}
