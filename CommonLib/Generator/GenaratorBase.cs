﻿using System;
using System.Diagnostics;

namespace CommonLib.Generator
{
    /// <summary>
    /// Базовый генератор для формирования исходных данных для задач и файла с ответами.
    /// </summary>
    public abstract class GenaratorBase
    {
        /// <summary>
        /// Каталог с файлами входными и ответами.
        /// </summary>
        private readonly string _path;

        /// <summary>
        /// Инициализация базового генератора с параметрами.
        /// </summary>
        /// <param name="path">Каталог, куда помещать сгенерированные файлы.</param>
        protected GenaratorBase(string path)
        {
            _path = path;
        }

        /// <summary>
        /// Выполнение генерации файлов со входными данными и файлов с решением (путем поиска его).
        /// </summary>
        public virtual void GenerateData(int testCount)
        {
            for (int i = 1; i <= testCount; i++)
            {
                // 1. генерация входных файлов для решения
                GenerateInputFile(i);

                // 2. выполнение поиска решщения для сгенерированных файлов
                var timer = new Stopwatch();
                timer.Start();
                GenerateAnswerFile(i);
                timer.Stop();

                Console.WriteLine("Calculation task for test #{0} estimated {1} ms.", i, timer.ElapsedMilliseconds);
            }
        }

        /// <summary>
        /// Генерация файла с заданием для заданного теста задачи.
        /// </summary>
        /// <param name="testNumber">Номер теста.</param>
        protected abstract void GenerateInputFile(int testNumber);

        /// <summary>
        /// Генерация файла с ответами по заданию из входного файла для задачи.
        /// </summary>
        /// <param name="testNumber">Номер теста.</param>
        protected abstract void GenerateAnswerFile(int testNumber);

        /// <summary>
        /// Получение имени файла для записи входных значений.
        /// </summary>
        /// <param name="number">Номер теста.</param>
        /// <returns>Возвращает сгенерированное имя.</returns>
        protected string GetInputFilename(int number)
        {
            var tmp = number.ToString().PadLeft(3, '0');
            return String.Format("{0}\\{1}.dat", _path, tmp);
        }

        /// <summary>
        /// Получение имени файла для записи ответа.
        /// </summary>
        /// <param name="number">Номер теста.</param>
        /// <returns>Возвращает сгенерированное имя.</returns>
        protected string GetAnswerFilename(int number)
        {
            var tmp = number.ToString().PadLeft(3, '0');
            return String.Format("{0}\\{1}.ans", _path, tmp);
        }
    }
}
