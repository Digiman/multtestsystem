﻿namespace CommonLib.Generator
{
    /// <summary>
    /// Базовый класс для реализации поиска решения для задач.
    /// </summary>
    public abstract class SolutionFinderBase
    {
        protected string Filename;

        protected SolutionFinderBase()
        { }

        protected SolutionFinderBase(string filename)
        {
            Filename = filename;
        }

        /// <summary>
        /// Выполнение поиска решения.
        /// </summary>
        public abstract void FindSolution();

        /// <summary>
        /// Запись результатов решения в файл с ответами.
        /// </summary>
        /// <param name="filename">Имя файла с ответами и путь к нему.</param>
        public abstract void WriteToFile(string filename);
    }
}
