﻿using CommonLib.Elements;
using CommonLib.Helpers;

namespace CommonLib
{
    /// <summary>
    /// Класс для реализации работы с файлом описания задачи для тестовой системы.
    /// </summary>
    public class TaskWorker
    {
        /// <summary>
        /// Опредение и описнаие задачи для тестовой системы.
        /// </summary>
        private TaskDefinition _definition;

        /// <summary>
        /// Инициализация объекта с данными.
        /// </summary>
        /// <param name="definition">Данные о задаче, определенные в другом месте.</param>
        public TaskWorker(TaskDefinition definition)
        {
            _definition = definition;
        }

        /// <summary>
        /// Инициализация объекта с данными, загружаемыми из файла.
        /// </summary>
        /// <param name="filename">Файл с данными о задаче.</param>
        public TaskWorker(string filename)
        {
            Load(filename);
        }

        /// <summary>
        /// Загрузка описания задачи из файла.
        /// </summary>
        /// <param name="filename">Файл с данными и путь к нему.</param>
        public void Load(string filename)
        {
            _definition = XmlWorker.LoadFromStatementFile(filename);
        }

        /// <summary>
        /// Запись данных в XML-файл.
        /// </summary>
        /// <param name="filename">Файл с данными и путь к нему для записи в него.</param>
        public void Save(string filename)
        {
            XmlWorker.GenerateStatementFile(filename, _definition);
        }
    }
}
