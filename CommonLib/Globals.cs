﻿namespace CommonLib
{
    public struct Globals
    {
        /// <summary>
        /// Название файла с описанием по умолчанию.
        /// </summary>
        public const string TasfDefinitionFileName = "statement.xml";

        /// <summary>
        /// Количество тестов по умолчанию.
        /// </summary>
        public const int TestCounts = 5;

        public const int MinHeight = 0;
        public const int MaxHeight = 255;

        public const int ParagraphMultiplier = 2000;
    }
}
