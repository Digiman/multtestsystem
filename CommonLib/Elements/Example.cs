﻿namespace CommonLib
{
    /// <summary>
    /// Пример для описания входов и выходов для задачи.
    /// </summary>
    public class Example
    {
        /// <summary>
        /// Входные данные.
        /// </summary>
        public string Input { get; set; }
        /// <summary>
        /// Выходные данные.
        /// </summary>
        public string Output { get; set; }
    }
}
