﻿using System.Collections.Generic;

namespace CommonLib.Elements
{
    /// <summary>
    /// Описание задачи для тестовой системы (statement.xml).
    /// </summary>
    public class TaskDefinition
    {
        /// <summary>
        /// Название пакета.
        /// </summary>
        public string ProblemPackage { get; set; }
        /// <summary>
        /// Идентификатор проблемы (по сути названеи задачи в системе)
        /// </summary>
        public string ProblemId { get; set; }
        /// <summary>
        /// Тип задачи (пакета).
        /// </summary>
        public string ProblemType { get; set; }
        /// <summary>
        /// Язык (в виде локали).
        /// </summary>
        public string StatementLanguage { get; set; }
        /// <summary>
        /// Название задачи.
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Описание задачи (может быть и HTML код).
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Набор примеров (данные для входа и выхода).
        /// </summary>
        public List<Example> Examples { get; set; }

        /// <summary>
        /// Инициализация "пустого" объекта.
        /// </summary>
        public TaskDefinition()
        {
            Examples = new List<Example>();

            // NOTE: Инициализация значенией по умолчанию
            ProblemPackage = "ru.ejudge.multic1test_contest";
            ProblemType = "standard";
            StatementLanguage = "ru-RU";
        }
    }
}
