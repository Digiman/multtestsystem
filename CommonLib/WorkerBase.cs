﻿using System.IO;
using System.Reflection.Emit;
using CommonLib.Elements;

namespace CommonLib
{
    public abstract class WorkerBase
    {
        /// <summary>
        /// Исполнитель для работы с файлом описания задачи (statement.xml).
        /// </summary>
        protected readonly TaskWorker _taskWorker;

        /// <summary>
        /// Катлог с данными для задачи.
        /// </summary>
        protected readonly string _folderName;

        /// <summary>
        /// Инициализация исполнителя с параметрами.
        /// </summary>
        /// <param name="folderName">Каталог, где будут размещены данные для задачи.</param>
        protected WorkerBase(string folderName)
        {
            _taskWorker = new TaskWorker(InitDefinition());

            _folderName = folderName;
        }

        /// <summary>
        /// Генерация всех данных по задаче.
        /// </summary>
        public abstract void GenerateAll();

        /// <summary>
        /// Инициализация данных для описания задачи.
        /// </summary>
        /// <returns>Возвращает файл с описанием задачи.</returns>
        protected abstract TaskDefinition InitDefinition();

        /// <summary>
        /// Поверка и создание каталога для размещения в ней тестов.
        /// </summary>
        /// <returns>Возвращает путь к каталогу для размещения файлов результатов работу генератора.</returns>
        protected string CheckAndCreateTaskDirectory()
        {
            var path = Path.GetFullPath(_folderName);

            var taskDir = Path.Combine(path, _folderName);
            if (!Directory.Exists(taskDir)) Directory.CreateDirectory(taskDir);

            return taskDir;
        }
    }
}