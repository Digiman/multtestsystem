﻿namespace ConsoleMainApp
{
    /// <summary>
    /// Класс для запуска всех заданий и отработки их по очереди.
    /// </summary>
    public class Tests
    {
        /// <summary>
        /// Запуск всех тестов (всех заданий).
        /// </summary>
        public static void RunTests()
        {
            RunTask1();
            RunTask2();
            RunTask3();
            RunTask4();
        }

        /// <summary>
        /// Запуск задачи №1.
        /// </summary>
        public static void RunTask1()
        {
            var tmp = new Task1.Worker("..\\..\\Tests\\task1");
            tmp.GenerateAll();
        }

        public static void RunTask2()
        {
            var tmp = new Task2.Worker("..\\..\\Tests\\task2");
            tmp.GenerateAll();
        }

        public static void RunTask3()
        {
            var tmp = new Task3.Worker("..\\..\\Tests\\task3");
            tmp.GenerateAll();
        }

        public static void RunTask4()
        {
            var tmp = new Task4.Worker("..\\..\\Tests\\task4");
            tmp.GenerateAll();
        }
    }
}