﻿using System;

namespace ConsoleMainApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Running all test...");

            try
            {
                Tests.RunTests();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: {0}", ex.Message);
            }

            Console.WriteLine("End executing all test...");
            Console.ReadKey();
        }
    }
}
