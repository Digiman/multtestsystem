﻿namespace Task1
{
    class Program
    {
        public static void Run()
        {
            Worker tmp = new Worker("task1");
            tmp.GenerateAll();
        }

        static void Main(string[] args)
        {
            Run();
        }
    }
}
