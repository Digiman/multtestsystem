﻿using System.IO;
using CommonLib;
using CommonLib.Elements;

namespace Task1
{
    /// <summary>
    /// Главный рабочий клас приложения для того, чтобы выполнить все операции.
    /// Исполнитель - выполняет создание всех требуемых файлов для помещения задачи в тестовую систему.
    /// </summary>
    public class Worker : WorkerBase
    {
        /// <summary>
        /// Инициализация исполнителя с параметрами.
        /// </summary>
        /// <param name="folderName">Каталог, где будут размещены данные для задачи.</param>
        public Worker(string folderName) : base(folderName)
        { }

        /// <summary>
        /// Генерация всех данных по задаче.
        /// </summary>
        public override void GenerateAll()
        {
            var taskDir = CheckAndCreateTaskDirectory();

            var filename = Path.Combine(taskDir, Globals.TasfDefinitionFileName);

            // 1. создание файла с описанием задачи
            _taskWorker.Save(filename);

            // 2. создание файлов с тестами и ответами для них
            var generator = new Generator(taskDir);
            generator.GenerateData(Globals.TestCounts);
        }

        /// <summary>
        /// Инициализация данных для описания задачи.
        /// </summary>
        /// <returns>Возвращает файл с описанием задачи.</returns>
        protected override TaskDefinition InitDefinition()
        {
            // NOTE: здесь вручную приводится описание задачи, ее параметры и прмиеры использования, которы затем пишутся в файл описания.

            var definition = new TaskDefinition();

            // описание проблемы
            definition.ProblemId = "A";

            // Описание задачи
            definition.Title = "Задача 'Сглаживание'";
            definition.Description = @"<p>Имеется двумерная матрица размерностью NxM, представляющая собой карту высот. Необходимо сформировать новую матрицу, где вместо значения высоты для каждой точки будет находится её среднее значение для окрестности радиусом R.</p>
		        <p>На первой строке ввода находятся 3 числа: N, M, R.</p>
		        <p>Далее идут N строк, по M чисел каждая.</p>
		        <p>Все числа не отрицательные, не превышают 240.</p>
		        <p>На выходе матрица NxM.</p>
		        <p>&nbsp;</p>
		        <p>Проверка попадания точки в окружность радиуса R, расположенной в начале координат:</p>
 		        <p><pre>(x*x + y*y) &lt; R*R</pre></p>";
            
            // примеры
            definition.Examples.Add(new Example
            {
                Input = @"5 5 2
                        0 0 0 0 0
                        0 3 5 3 0
                        0 5 7 5 0
                        0 3 5 3 0
                        0 0 0 0 0",
                Output = @"0 1 1 1 0 
                        1 2 3 2 1 
                        1 3 4 3 1 
                        1 2 3 2 1 
                        0 1 1 1 0"
            });

            return definition;
        }
    }
}
