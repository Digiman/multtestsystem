﻿# Задача №1

Имеется двумерная матрица размерностью NxM, представляющая собой карту высот. Необходимо сформировать новую матрицу, где вместо значения высоты для каждой точки будет находится её среднее значение для окрестности радиусом R.

1. На первой строке ввода находятся 3 числа: N, M, R.

2. Далее идут N строк, по M чисел каждая.

3. Все числа не отрицательные, не превышают 240.

4. На выходе матрица NxM.

Проверка попадания точки в окружность радиуса R, расположенной в начале координат:

```(x*x + y*y) <= R*R```   
