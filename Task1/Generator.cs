﻿using System;
using System.IO;
using CommonLib;
using CommonLib.Generator;

namespace Task1
{
    /// <summary>
    /// Генератор для задачи 1.
    /// </summary>
    public class Generator : GenaratorBase
    {
        /// <summary>
        /// Инициализация генератора входных и выходных файлов для задачи.
        /// </summary>
        /// <param name="path">Путь к кателогу, где будут размещаться файлы.</param>
        public Generator(string path) : base(path)
        {
        }

        /// <summary>
        /// Генерация входного файла с данными длоя решения задачи.
        /// </summary>
        /// <param name="testNumber">Номер теста.</param>
        protected override void GenerateInputFile(int testNumber)
        {
            var filename = GetInputFilename(testNumber);

            var file = new StreamWriter(filename);

            int N = testNumber*30;
            int M = N;
            int R = N/2;
            file.WriteLine("{0} {1} {2}", N, M, R);

            int[,] matrix = new int[N, M];

            Random r = new Random(1000);

            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < M; j++)
                {
                    matrix[i, j] = (r.Next()%Globals.MaxHeight) + Globals.MinHeight;
                    file.Write("{0} ", matrix[i, j]);
                }
                file.Write("\n");
            }

            file.Close();
        }

        /// <summary>
        /// Генерация файла с ответами путем решения задачи.
        /// </summary>
        /// <param name="testNumber">Номер теста.</param>
        protected override void GenerateAnswerFile(int testNumber)
        {
            var inputfile = GetInputFilename(testNumber);
            var finder = new SolutionFinder(inputfile);
            finder.FindSolution();
            finder.WriteToFile(GetAnswerFilename(testNumber));
        }
    }
}
