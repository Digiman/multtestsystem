﻿using System;
using System.IO;
using CommonLib.Generator;
using CommonLib.Helpers;

namespace Task1
{
    /// <summary>
    /// Класс для реализации поиска решения для рассматриваемой задачи.
    /// </summary>
    public class SolutionFinder : SolutionFinderBase
    {
        /// <summary>
        /// Размерность матрицы 1 (столбцов).
        /// </summary>
        private int M;
        /// <summary>
        /// Размерность матрицы 2 (строк).
        /// </summary>
        private int N;
        /// <summary>
        /// Радиус окружности.
        /// </summary>
        private int R;

        /// <summary>
        /// Исходная матрица
        /// </summary>
        private int[,] A;
        /// <summary>
        /// Матрица с результатами.
        /// </summary>
        private int[,] B;

        public SolutionFinder(int[,] matrix, int r)
        {
            M = matrix.GetLength(0);
            N = matrix.GetLength(1);
            R = r;
            A = matrix;
            B = new int[N, M];
        }

        /// <summary>
        /// Инициаизация решателя путем передачи ему имени файла с исходынми данными.
        /// </summary>
        /// <param name="filename">Файл и путь к нему с исходными (входными) данными.</param>
        public SolutionFinder(string filename) : base(filename)
        {
            ReadInputFile(filename);
            B = new int[N, M];
        }

        /// <summary>
        /// Выполнение поиска решения.
        /// </summary>
        public override void FindSolution()
        {
            int x, y;
            for (x = 0; x < N; x++)
                for (y = 0; y < M; y++)
                    B[x, y] = CalcDot(x, y);
        }

        /// <summary>
        /// Поиск точек в окружности.
        /// </summary>
        /// <param name="cx">Центр окружности по X.</param>
        /// <param name="cy">Центр окружности по Y.</param>
        /// <returns>Возвращает среднее значение по суммам площадей, которые попадабт в окружность с заданным радиусом.</returns>
        private int CalcDot(int cx, int cy)
        {
            int i, j, x, y;
            int xB, xE, yB, yE;
            int sum;
            int cnt;

            xB = cx - R;
            if (xB < 0) xB = 0;

            xE = cx + R + 1;
            if (xE > N) xE = N;

            yB = cy - R;
            if (yB < 0) yB = 0;

            yE = cy + R + 1;
            if (yE > M) yE = M;

            sum = 0;
            cnt = 0;
            for (i = xB; i < xE; i++)
                for (j = yB; j < yE; j++)
                {
                    x = i - cx;
                    y = j - cy;
                    if ((x*x + y*y) < R*R)
                    {
                        sum += A[i,j];
                        cnt ++;
                    }
                }
            return sum/cnt;
        }

        /// <summary>
        /// Запись результатов решения в файл с ответами.
        /// </summary>
        /// <param name="filename">Имя файла с ответами и путь к нему.</param>
        public override void WriteToFile(string filename)
        {
            FileWorker.WriteDataToFile(B, filename);
        }

        /// <summary>
        /// Чтение данных из входного файла.
        /// </summary>
        /// <param name="filename">Файл с даными и путь к нему.</param>
        public void ReadInputFile(string filename)
        {
            var file = new StreamReader(filename);

            var line = file.ReadLine();
            var mas = line.Split(' ');
            N = Convert.ToInt32(mas[0]);
            M = Convert.ToInt32(mas[1]);
            R = Convert.ToInt32(mas[2]);

            A = new int[N, M];

            for (int i = 0; i < N; i++)
            {
                line = file.ReadLine();
                var lineData = line.Split(' ');
                for (int j = 0; j < M; j++)
                {
                    A[i, j] = Convert.ToInt32(lineData[j]);
                }
            }

            file.Close();
        }
    }
}
