﻿using System.IO;
using CommonLib;
using CommonLib.Elements;

namespace Task3
{
    /// <summary>
    /// Главный рабочий клас приложения для того, чтобы выполнить все операции.
    /// Исполнитель - выполняет создание всех требуемых файлов для помещения задачи в тестовую систему.
    /// </summary>
    public class Worker : WorkerBase
    {
        /// <summary>
        /// Инициализация исполнителя с параметрами.
        /// </summary>
        /// <param name="folderName">Каталог, где будут размещены данные для задачи.</param>
        public Worker(string folderName) : base(folderName)
        { }

        /// <summary>
        /// Генерация всех данных по задаче.
        /// </summary>
        public override void GenerateAll()
        {
            var taskDir = CheckAndCreateTaskDirectory();

            var filename = Path.Combine(taskDir, Globals.TasfDefinitionFileName);

            // 1. создание файла с описанием задачи
            _taskWorker.Save(filename);

            // 2. создание файлов с тестами и ответами для них
            var generator = new Generator(taskDir);
            generator.GenerateData(Globals.TestCounts);
        }

        /// <summary>
        /// Инициализация данных для описания задачи.
        /// </summary>
        /// <returns>Возвращает файл с описанием задачи.</returns>
        protected override TaskDefinition InitDefinition()
        {
            // NOTE: здесь вручную приводится описание задачи, ее параметры и прмиеры использования, которы затем пишутся в файл описания.

            var definition = new TaskDefinition();

            // описание проблемы
            definition.ProblemId = "E";

            // Описание задачи
            definition.Title = "Задача о подсчете слов";
            definition.Description = @"В тектовом файле заменить каждое слово числом, показывающим сколько данное слово встречается в текстовом файле. Различные формы одного и того же слова считать разными словами.";
            
            // примеры
            definition.Examples.Add(new Example
            {
                Input = "Et kasd vel kasd et amet sea hendrerit aliquyam accusam elitr accusam. Sanctus ut lorem eirmod wisi rebum dolor elit dolore amet nonumy at eos ut hendrerit amet eum. Stet dolores gubergren sit nibh amet. Amet eros eos eros eros lorem iriure justo takimata eirmod amet stet consetetur sed magna. Molestie accusam et nulla liber minim consequat ea feugiat dolores sadipscing. Lorem ipsum est sed et exerci dolore molestie vero et tincidunt veniam. Suscipit ea erat quod dolore accusam et dolore invidunt et nibh gubergren lorem. Lorem voluptua in aliquyam no voluptua dolores luptatum. Et sanctus zzril ad clita consetetur euismod rebum ea consectetuer tempor. Nobis ipsum gubergren amet amet sea dolor dolor ea kasd ut sit tempor consequat kasd lobortis sadipscing ea.",
                Output = "2 4 1 4 6 7 2 2 2 4 1 4 1 3 3 2 1 2 3 1 4 7 1 1 2 3 2 7 1 1 3 3 2 2 7 1 3 2 3 3 3 1 1 1 2 7 1 2 2 1 1 4 6 1 1 1 2 5 1 3 2 2 2 1 2 6 1 4 1 1 6 1 1 1 5 1 1 4 4 6 4 1 6 2 3 3 2 2 1 2 1 2 3 1 2 1 1 1 1 2 1 2 5 1 2 1 2 3 7 7 2 3 3 5 4 3 2 2 2 4 1 2 5 "
            });

            return definition;
        }
    }
}
