﻿namespace Task3
{
    class Program
    {
        public static void Run()
        {
            Worker tmp = new Worker("task3");
            tmp.GenerateAll();
        }

        static void Main(string[] args)
        {
            Run();
        }
    }
}
