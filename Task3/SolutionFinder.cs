﻿using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using CommonLib.Generator;

namespace Task3
{
    /// <summary>
    /// Класс для реализации поиска решения для рассматриваемой задачи №3.
    /// </summary>
    public class SolutionFinder : SolutionFinderBase
    {
        /// <summary>
        /// Весь текст из файла с входыными данными.
        /// </summary>
        private string _fileData;

        /// <summary>
        /// Набор слов из файла. Слова идут в порядке их появления в файле.
        /// </summary>
        private List<string> _words;

        /// <summary>
        /// Словарь с набором уникальных слов в предложении с указанием их количества повторений.
        /// </summary>
        private Dictionary<string, int> _dictionary;

        /// <summary>
        /// Инициаизация решателя путем передачи ему имени файла с исходынми данными.
        /// </summary>
        /// <param name="filename">Файл и путь к нему с исходными (входными) данными.</param>
        public SolutionFinder(string filename) : base(filename)
        {
            ReadInputFile(filename);
        }

        /// <summary>
        /// Выполнение поиска решения.
        /// </summary>
        public override void FindSolution()
        {
            _dictionary = new Dictionary<string, int>();

            _words = ParseText(_fileData);

            foreach (var word in _words)
            {
                if (!_dictionary.ContainsKey(word))
                    _dictionary.Add(word, 1);
                else
                {
                    _dictionary[word]++;
                }
            }
        }

        /// <summary>
        /// Разбор текста на слова.
        /// </summary>
        /// <param name="text">Входной текст из файла.</param>
        /// <returns>Возвращает набор слов, которые были получены из текста.</returns>
        private List<string> ParseText(string text)
        {
            var words = new List<string>();
            var regex = new Regex("\r\n");
            var pars = regex.Split(text);
            foreach (var par in pars)
            {
                var sentences = par.Split('.');
                foreach (var sent in sentences)
                {
                    words.AddRange(sent.Split(' '));
                }
            }

            words = CleanWords(words);

            return words;
        }

        /// <summary>
        /// Удаление пустых слов из набора.
        /// </summary>
        /// <param name="words">Слова для очистки.</param>
        /// <returns>Возвращает набор слов после очистки.</returns>
        private List<string> CleanWords(List<string> words)
        {
            words.RemoveAll(w => w == "");
            return words;
        }

        /// <summary>
        /// Запись результатов решения в файл с ответами.
        /// </summary>
        /// <param name="filename">Имя файла с ответами и путь к нему.</param>
        public override void WriteToFile(string filename)
        {
            var file = new StreamWriter(filename);

            foreach (var word in _words)
            {
                file.Write("{0} ", _dictionary[word]);
            }

            file.Close();
        }

        /// <summary>
        /// Чтение данных из входного файла.
        /// </summary>
        /// <param name="filename">Файл с даными и путь к нему.</param>
        private void ReadInputFile(string filename)
        {
            var file = new StreamReader(filename);

            file.ReadLine();
            _fileData = file.ReadToEnd();

            file.Close();
        }
    }
}
