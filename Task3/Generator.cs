﻿using System.IO;
using CommonLib;
using CommonLib.Generator;

namespace Task3
{
    /// <summary>
    /// Генератор для задачи 3.
    /// </summary>
    public class Generator : GenaratorBase
    {
        /// <summary>
        /// Инициализация генератора входных и выходных файлов для задачи.
        /// </summary>
        /// <param name="path">Путь к кателогу, где будут размещаться файлы.</param>
        public Generator(string path) : base(path)
        {
        }

        /// <summary>
        /// Генерация входного файла с данными длоя решения задачи.
        /// </summary>
        /// <param name="testNumber">Номер теста.</param>
        protected override void GenerateInputFile(int testNumber)
        {
            var filename = GetInputFilename(testNumber);
            var lines = testNumber*Globals.ParagraphMultiplier;

            var file = new StreamWriter(filename);

            var gen = new NLipsum.Core.LipsumGenerator();
            var text = gen.GenerateParagraphs(lines);

            file.WriteLine(lines);
            foreach (var par in text)
            {
                file.WriteLine(par);
            }

            file.Close();
        }

        /// <summary>
        /// Генерация файла с ответами путем решения задачи.
        /// </summary>
        /// <param name="testNumber">Номер теста.</param>
        protected override void GenerateAnswerFile(int testNumber)
        {
            var inputfile = GetInputFilename(testNumber);
            var finder = new SolutionFinder(inputfile);
            finder.FindSolution();
            finder.WriteToFile(GetAnswerFilename(testNumber));
        }
    }
}
